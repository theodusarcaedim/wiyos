﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WIYOS
{
    public class UnloadWithMasterScene : MonoBehaviour
    {
        private string _masterScenePath;
        public void Init(string masterScenePath)
        {
            _masterScenePath = masterScenePath;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void OnSceneUnloaded(Scene scene)
        {
            // If the scene this object belonged to was just unloaded, this object will be null.
            if (null == this || null == gameObject)
            {
                return;
            }

            if (0 == string.Compare(scene.path, _masterScenePath))
            {
                string sceneName = scene.name.SceneNameFromFullPath();
                string thisSceneName = gameObject.scene.name.SceneNameFromFullPath();
                Debug.LogFormat("Unload of master scene {0} detected: also unloading scene {1}", sceneName, thisSceneName);
            }
        }
    }
}