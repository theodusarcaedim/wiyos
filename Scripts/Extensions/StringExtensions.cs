﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WIYOS
{
    public static class StringExtensions
    {
        public static string SceneNameFromFullPath(this string str)
        {
            string sceneName = str.Substring(str.LastIndexOf('/') + 1);
            sceneName = sceneName.Remove(sceneName.Length - 6); // ".scene"
            return sceneName;
        }
    }
}