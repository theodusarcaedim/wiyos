﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WIYOS
{
    public class MasterSceneEntry : MonoBehaviour
    {
        [SerializeField, ScenePath]
        private string masterScene;
        [SerializeField]
        private CompositeSceneEntry[] sceneList;

        public string MasterScene { get { return masterScene; } }
        public CompositeSceneEntry[] SceneList { get { return sceneList; } }

        private void Awake()
        {
            for (int i = 0; i < sceneList.Length; i++)
            {
                CompositeSceneEntry sceneEntry = sceneList[i];
                StartCoroutine(LoadCompositeScene(sceneEntry));
            }
        }

        IEnumerator LoadCompositeScene(CompositeSceneEntry sceneEntry)
        {
            Debug.LogFormat("Master scene {0}: loading composite scene {1}", masterScene.SceneNameFromFullPath(), sceneEntry.scenePath.SceneNameFromFullPath());
            AsyncOperation op = SceneManager.LoadSceneAsync(sceneEntry.scenePath, LoadSceneMode.Additive);
            while (!op.isDone)
            {
                yield return null;
            }
            Debug.LogFormat("Composite scene {0} loading complete", sceneEntry.scenePath.SceneNameFromFullPath());
            Scene newScene = SceneManager.GetSceneByPath(sceneEntry.scenePath);
            if (!newScene.IsValid())
            {
                Debug.LogErrorFormat("Scene {0} is missing from SceneManager", sceneEntry.scenePath.SceneNameFromFullPath());
                yield break;
            }
            else
            {
                if (sceneEntry.useLighting)
                {
                    Debug.LogFormat("Using lighting settings for scene {0}", sceneEntry.scenePath.SceneNameFromFullPath());
                    SceneManager.SetActiveScene(newScene);
                }

                Debug.LogFormat("Scene {0} will unload when scene {1} is unloaded", sceneEntry.scenePath.SceneNameFromFullPath(), masterScene.SceneNameFromFullPath());
                GameObject unloader = new GameObject("==SCENE UNLOADER==");
                UnloadWithMasterScene unloadScript = unloader.AddComponent<UnloadWithMasterScene>();
                unloadScript.Init(masterScene);
                SceneManager.MoveGameObjectToScene(unloader, newScene);
            }
        }
    }
}