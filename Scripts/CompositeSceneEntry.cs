﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WIYOS
{
    [System.Serializable]
    public class CompositeSceneEntry
    {
        [ScenePath, Tooltip("The scene asset.")]
        public string scenePath;
        [Tooltip("True if this is the scene whose lighting settings you wish to use.")]
        public bool useLighting;
        [Tooltip("True if new objects should be spawned into this scene.")]
        public bool spawnObjectsHere;
    }
}