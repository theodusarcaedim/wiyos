﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace WIYOS
{
    [InitializeOnLoad]
    public class SceneMappingEditorHooks
    {
        static SceneMappingEditorHooks()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            switch (state)
            {
                case PlayModeStateChange.ExitingEditMode:
                    {
                        MasterSceneEntry[] masterSceneEntries = GameObject.FindObjectsOfType<MasterSceneEntry>();
                        if (null == masterSceneEntries || 0 == masterSceneEntries.Length)
                        {
                            Debug.LogError("OnPlayModeStateChanged found no MasterSceneEntry objects in any loaded scenes!");
                            return;
                        }

                        List<EditorBuildSettingsScene> editorScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
                        List<string> editorScenePaths = new List<string>(editorScenes.Select(s => s.path));
                        List<string> scenePathsToAdd = new List<string>();
                        for (int i = 0; i < masterSceneEntries.Length; i++)
                        {
                            MasterSceneEntry entry = masterSceneEntries[i];
                            if (!editorScenePaths.Contains(entry.MasterScene))
                            {
                                Debug.LogWarningFormat("Adding scene {0} to EditorBuildSettings", entry.MasterScene);
                                scenePathsToAdd.Add(entry.MasterScene);
                            }
                            for (int j = 0; j < entry.SceneList.Length; j++)
                            {
                                if (!editorScenePaths.Contains(entry.SceneList[j].scenePath))
                                {
                                    Debug.LogWarningFormat("Adding scene {0} to EditorBuildSettings", entry.SceneList[j].scenePath);
                                    scenePathsToAdd.Add(entry.SceneList[j].scenePath);
                                }
                            }
                        }
                        for (int i = 0; i < scenePathsToAdd.Count; i++)
                        {
                            editorScenes.Add(new EditorBuildSettingsScene(scenePathsToAdd[i], true));
                        }
                        EditorBuildSettings.scenes = editorScenes.ToArray();

                    }
                    break;
            };
        }
    }
}